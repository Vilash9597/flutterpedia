import 'package:flutter/material.dart';
/**
 * Meditab Software Inc. CONFIDENTIAL
 * __________________
 *
 *  [2018] Meditab Software Inc.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Meditab Software Inc. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Meditab Software Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Meditab Software Incorporated.

 * <h1>terms_screen</h1>
 *
 * <p>
 *
 * @author Vilashraj Patel (vilashp@meditab.com) Meditab Software Inc.
 * @version 1.0
 * @since 2019-06-01 11:42
 */

class Terms extends StatefulWidget {
  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[

        ],
      ),
    );
  }
}
